#!/usr/bin/env python3
# coding=UTF-8
import random, math
from functools import reduce

LIST_DATA = [ 4.1, 9, 0, "coucou", -1000, 1000, [ 0, 1], "42", -float("inf"), 18, -49, -4999.0, -455, float("inf"), -541, 9999.9, float("nan") ]
LIST_DATA.extend(4.2 * random.randint(-1000,1000) for r in range(20))
CHAINE = "abcd0ef9ghij8klmA5BCD7EFG6HIJK4LMNO3PQR2STU1VWXYZnopqrstuvwxyz"

# Exercise 3.1: Filtering list data
# Write a lambda filter to select only integers and real numbers

LIST_DATA1 = list(filter(lambda x : isinstance(x, (int, float)), LIST_DATA))


# Exercise 3.2:  Filtering list data
# Write a lambda filter to select numbers in the range [-1000 et 1000[

LIST_DATA2 = list(filter(lambda x : -1000 <= x < 1000, LIST_DATA1))


# Exercise 3.3: Mapping list data
# Write a lambda mapping that transoform LIST_DATA2 to absolute values
LIST_DATA3 = list(map(lambda x : x if x >= 0 else -x, LIST_DATA2))
	
# Exercise 3.4: Reducing list data
# compute the multiplication of elements in LIST_DATA3 (as integers, 0 must be transformed to 1)
def toPositiveInt(x):
	x = int(x)
	return x if x != 0 else 1
LIST_DATA4 = reduce(lambda a, b : toPositiveInt(a) * toPositiveInt(b), LIST_DATA3)

# Exercise 3.5: Filtering strings
# Write a lambda filter to select characters in CHAINE between ['e'..'n'] and  ['E'..'N']
CHAINE5 = ''.join(filter(lambda x : 'e' <= x.lower() <= 'n', CHAINE))









#*****************************************************************************#
#*****************************************************************************#
#***                                Checks                                 ***#
#*****************************************************************************#
#*****************************************************************************#


print("\nChecking 3.1")
hasError = False
for e in LIST_DATA1:
	if not (isinstance(e, int) or isinstance(e, float)):
		print("  - ", e, "is neither an integer nor a float")
		hasError = True
if hasError:
	print("You should correct your errors.")
else:
	print("Congratulations, your filter behave as expected.")

print("\nChecking 3.2")
hasError = False
for e in LIST_DATA2:
	if isinstance(e, int) or isinstance(e, float):
		if math.isnan(e) or e >= 1000 or e < -1000:
			print("  - ", e, "is not in the interval [-1000..1000[")
			hasError = True
if hasError:
	print("You should correct your errors.")
else:
	print("Congratulations, your filter behave as expected.")

print("\nChecking 3.3")
hasError = len(LIST_DATA3) != len(LIST_DATA2)
for e in LIST_DATA3:
	if isinstance(e, int) or isinstance(e, float):
		if (not math.isnan(e)) and (-1000<e<=1000):
			if e < 0:
				print("  - ", e, "is not >= 0")
				hasError = True
if hasError:
	print("You should correct your errors.")
else:
	print("Congratulations, your mapping behave as expected.")

print("\nChecking 3.4")
verif = 1
for e in LIST_DATA3:
	if (isinstance(e, int) or isinstance(e, float)) and (not math.isnan(e)) and (0<=e<=1000):
		e = int(e)
		verif = verif * (1 if (e == 0) else e)
if int(LIST_DATA4) != int(verif):
	print("You should correct your errors.")
else:
	print("Congratulations, your reduction behave as expected.")

print("\nChecking 3.5")
if CHAINE5 == 'efghijklmEFGHIJKLMNn':
	print("Congratulations, your filter behave as expected.")
else:
	print("You should correct your errors, '{}' != '{}'".format(CHAINE5, 'efghijklmEFGHIJKLMNn'))

