# Set-up your Python envirnonment

## Linux

### Install
- recent distribution : install using your package manager: jupyter-notebook spyder3 ipython3 python3-numy python3-matplotlib
- old distribution : install using your package manager: ipython3-notebook spyder3 ipython3 python3-numy python3-matplotlib

### Execution
- put your terminal in the folder containing the "*.ipynb"
- run the command jupyter notebook (or ipython3 notebook for old distributions)
- a web browser is open and allows you to select your notebook.

## Windows and MacOS (anaconda distribution)

### Install
- Download the lastest 3.X version for your operating system from https://www.anaconda.com/download/
- Install the downloaded file

### Execution
- see the anaconda documentation or  your favorite search engine

## Temporary jupyter Cloud server

The courses and practical works are available on this repository : https://gitlab.com/sdiakite/tmpcp you can access the notebook on the cloud here : https://mybinder.org/v2/gl/sdiakite%2Ftmpcp/master


## Extra : allow "bash" notebooks in Jupyter

**Only tested in Linux**

Install the bash kernel for jupyter : https://github.com/takluyver/bash_kernel

Python 3:
 - `pip3 install bash_kernel`
 - `python3 -m bash_kernel.install`
